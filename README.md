# The Fast Kalman Filer Toolbox

The Fast Kalman Fileter Toolbox for R allows the implimentation of
multivariate time series filtering with external time varying inputs using the
linear Kalman fileter in R.

This repository contains the existing and developmental source code for the
package starting with version 0.1.5.

The canonical lin to the package on CRAN whcih includes examples and
documentation is https://CRAN.R-project.org/package=FKF




